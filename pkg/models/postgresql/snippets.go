package postgresql

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"se1909.com/pkg/models"
	"strconv"
	"time"
)

type SnippetModel struct {
	DB *pgxpool.Pool
}

func (m *SnippetModel) Insert(title, content, expires string) (int, error) {
	stmt := "insert into snippetbox.snippets (title,content,created,expires) VALUES ($1, $2, $3, $4) RETURNING id"
	intExpires, err := strconv.Atoi(expires)
	if err != nil {
		return 0, err
	}

	var lastIndex int
	err = m.DB.QueryRow(context.Background(), stmt, title, content, time.Now(), time.Now().AddDate(0, 0, intExpires)).Scan(&lastIndex)
	if err != nil {
		return 0, err
	}
	return int(lastIndex), nil
}

func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	stmt := "Select id, title, content, created, expires FROM snippetbox.snippets WHERE expires > CLOCK_TIMESTAMP() AND id=$1"

	row := m.DB.QueryRow(context.Background(), stmt, id)

	s := &models.Snippet{}

	err := row.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return s, nil
}

func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	stmt := "Select id, title, content, created, expires FROM snippetbox.snippets WHERE expires > CLOCK_TIMESTAMP() ORDER BY created DESC LIMIT 10"

	rows, err := m.DB.Query(context.Background(), stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	snippets := []*models.Snippet{}

	for rows.Next() {
		s := &models.Snippet{}

		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}

		snippets = append(snippets, s)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}
	return snippets, nil
}
